<?php

declare(strict_types=1);

namespace Bdrops\CMS\Listener;

use Bdrops\CMS\Services\PageService;

abstract class PageBaseListener
{
    /**
     * @var PageService
     */
    protected $pageService;

    /**
     * PageBaseListener constructor.
     *
     * @param PageService $pageService
     */
    public function __construct(PageService $pageService)
    {
        $this->pageService = $pageService;
    }
}
