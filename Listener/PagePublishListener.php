<?php

declare(strict_types=1);

namespace Bdrops\CMS\Listener;

use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\ListenerInterface;
use Bdrops\CQRS\Services\CommandBus;

class PagePublishListener extends PageBaseListener implements ListenerInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(CommandBus $commandBus, EventInterface $event): void
    {
        $payload = $event->getCommand()->getPayload();
        $pageUuid = $event->getCommand()->getAggregateUuid();

        if ($payload && $payload['version']) {
            $version = (int) $payload['version'];

            $this->pageService->publishPage($pageUuid, $version);
        }
    }
}
