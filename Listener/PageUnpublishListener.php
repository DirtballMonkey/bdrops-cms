<?php

declare(strict_types=1);

namespace Bdrops\CMS\Listener;

use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\ListenerInterface;
use Bdrops\CQRS\Services\CommandBus;

class PageUnpublishListener extends PageBaseListener implements ListenerInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(CommandBus $commandBus, EventInterface $event): void
    {
        $pageUuid = $event->getCommand()->getAggregateUuid();
        $this->pageService->unpublishPage($pageUuid);
    }
}
