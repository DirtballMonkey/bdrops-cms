<?php

declare(strict_types=1);

namespace Bdrops\CMS\Event;

use Bdrops\CMS\Command\PageAddElementCommand;
use Bdrops\CMS\Listener\PageAddElementListener;
use Bdrops\CQRS\Event\Event;
use Bdrops\CQRS\Interfaces\EventInterface;

final class PageAddElementEvent extends Event implements EventInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return PageAddElementCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getListenerClass(): string
    {
        return PageAddElementListener::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): string
    {
        return 'Element added to Page';
    }

    /**
     * {@inheritdoc}
     */
    public static function getCode(): int
    {
        return CODE_OK;
    }
}
