<?php

declare(strict_types=1);

namespace Bdrops\CMS\Event;

use Bdrops\CMS\Command\PageCloneCommand;
use Bdrops\CMS\Listener\PageCloneListener;
use Bdrops\CQRS\Event\Event;
use Bdrops\CQRS\Interfaces\EventInterface;

final class PageCloneEvent extends Event implements EventInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return PageCloneCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getListenerClass(): string
    {
        return PageCloneListener::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): string
    {
        return 'Page Cloned';
    }

    /**
     * {@inheritdoc}
     */
    public static function getCode(): int
    {
        return CODE_CREATED;
    }
}
