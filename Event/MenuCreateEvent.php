<?php

declare(strict_types=1);

namespace Bdrops\CMS\Event;

use Bdrops\CMS\Command\MenuCreateCommand;
use Bdrops\CMS\Listener\MenuCreateListener;
use Bdrops\CQRS\Event\Event;
use Bdrops\CQRS\Interfaces\EventInterface;

final class MenuCreateEvent extends Event implements EventInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return MenuCreateCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getListenerClass(): string
    {
        return MenuCreateListener::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): string
    {
        return 'Menu Created';
    }

    /**
     * {@inheritdoc}
     */
    public static function getCode(): int
    {
        return CODE_CREATED;
    }
}
