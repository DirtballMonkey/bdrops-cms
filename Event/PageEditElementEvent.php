<?php

declare(strict_types=1);

namespace Bdrops\CMS\Event;

use Bdrops\CMS\Command\PageEditElementCommand;
use Bdrops\CMS\Listener\PageEditElementListener;
use Bdrops\CQRS\Event\Event;
use Bdrops\CQRS\Interfaces\EventInterface;

final class PageEditElementEvent extends Event implements EventInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return PageEditElementCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getListenerClass(): string
    {
        return PageEditElementListener::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): string
    {
        return 'Element edited';
    }

    /**
     * {@inheritdoc}
     */
    public static function getCode(): int
    {
        return CODE_OK;
    }
}
