<?php

declare(strict_types=1);

namespace Bdrops\CMS\Form\Menu;

use Symfony\Component\Form\FormBuilderInterface;

class Divider extends Item
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }
}
