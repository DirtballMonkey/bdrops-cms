<?php

declare(strict_types=1);

namespace Bdrops\CMS\Form\Elements;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class Section extends Element
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('section', TextType::class, [
            'label' => 'Choose to which layout section this section belongs.',
        ]);
    }
}
