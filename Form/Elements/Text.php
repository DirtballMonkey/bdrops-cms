<?php

declare(strict_types=1);

namespace Bdrops\CMS\Form\Elements;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class Text extends Element
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('text', TextareaType::class, [
            'label' => 'Please insert the text you want to show.',
            'attr' => [
                'class' => 'ckeditor',
            ],
        ]);
    }
}
