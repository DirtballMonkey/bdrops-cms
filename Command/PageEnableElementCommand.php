<?php

declare(strict_types=1);

namespace Bdrops\CMS\Command;

use Bdrops\CMS\Handler\PageEnableElementHandler;
use Bdrops\CMS\Model\Page;
use Bdrops\CQRS\Command\Command;
use Bdrops\CQRS\Interfaces\CommandInterface;

final class PageEnableElementCommand extends Command implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function getHandlerClass(): string
    {
        return PageEnableElementHandler::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getAggregateClass(): string
    {
        return Page::class;
    }
}
