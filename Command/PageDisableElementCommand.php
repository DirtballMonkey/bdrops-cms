<?php

declare(strict_types=1);

namespace Bdrops\CMS\Command;

use Bdrops\CMS\Handler\PageDisableElementHandler;
use Bdrops\CMS\Model\Page;
use Bdrops\CQRS\Command\Command;
use Bdrops\CQRS\Interfaces\CommandInterface;

final class PageDisableElementCommand extends Command implements CommandInterface
{
    /**
     * {@inheritdoc}
     */
    public function getHandlerClass(): string
    {
        return PageDisableElementHandler::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getAggregateClass(): string
    {
        return Page::class;
    }
}
