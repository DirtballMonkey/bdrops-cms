<?php

declare(strict_types=1);

namespace Bdrops\CMS\Handler;

use Bdrops\CMS\Command\PageUnpublishCommand;
use Bdrops\CMS\Event\PageUnpublishEvent;
use Bdrops\CMS\Model\Page;
use Bdrops\CQRS\Interfaces\AggregateInterface;
use Bdrops\CQRS\Interfaces\CommandInterface;
use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\HandlerInterface;

final class PageUnpublishHandler extends PageBaseHandler implements HandlerInterface
{
    /**
     * {@inheritdoc}
     *
     * @var Page $aggregate
     */
    public function execute(CommandInterface $command, AggregateInterface $aggregate): AggregateInterface
    {
        $aggregate->published = false;

        return $aggregate;
    }

    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return PageUnpublishCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public function createEvent(CommandInterface $command): EventInterface
    {
        return new PageUnpublishEvent($command);
    }

    /**
     * {@inheritdoc}
     */
    public function validateCommand(CommandInterface $command, AggregateInterface $aggregate): bool
    {
        return true;
    }
}
