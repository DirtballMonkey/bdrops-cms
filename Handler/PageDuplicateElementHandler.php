<?php

declare(strict_types=1);

namespace Bdrops\CMS\Handler;

use Bdrops\CMS\Command\PageDuplicateElementCommand;
use Bdrops\CMS\Event\PageDuplicateElementEvent;
use Bdrops\CMS\Model\Page;
use Bdrops\CQRS\Interfaces\AggregateInterface;
use Bdrops\CQRS\Interfaces\CommandInterface;
use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\HandlerInterface;
use Bdrops\CQRS\Message\Message;
use Ramsey\Uuid\Uuid;

final class PageDuplicateElementHandler extends PageBaseHandler implements HandlerInterface
{
    private function assignNewUuid(array $element, string $commandUuid): array
    {
        // Convert old uuid to hash (in 16char bytes) and use it as a seed.
        $seed = $commandUuid.'-'.$element['uuid'];
        $seed = md5($seed, true);

        $newUuid = Uuid::fromBytes($seed)->toString();

        // Set new Uuid.
        $element['uuid'] = $newUuid;

        if (isset($element['elements']) && is_array($element['elements'])) {
            foreach ($element['elements'] as $key => $subElement) {
                $element['elements'][$key] = $this->assignNewUuid($subElement, $commandUuid);
            }
        }

        return $element;
    }

    /**
     * {@inheritdoc}
     *
     * @var Page $aggregate
     */
    public function execute(CommandInterface $command, AggregateInterface $aggregate): AggregateInterface
    {
        $payload = $command->getPayload();

        $commandUuid = $command->getUuid();
        $uuid = $payload['uuid'];

        // A function that duplicates all matching elements.
        $duplicateFunction = function (&$element, &$collection) use ($uuid, $commandUuid) {
            if (null !== $collection) {
                // Get the key of the item that will duplicate.
                $itemKey = null;
                foreach ($collection as $key => $subElement) {
                    if ($subElement['uuid'] === $uuid) {
                        $itemKey = $key;
                        continue;
                    }
                }

                if (null !== $itemKey) {
                    // Copy element.
                    $duplicatedElement = $element;

                    // Set new uuids to element and subelements.
                    $duplicatedElement = $this->assignNewUuid($duplicatedElement, $commandUuid);

                    // Insert duplicated element into collection.
                    array_splice($collection, ($itemKey + 1), 0, [$duplicatedElement]);
                }
            }
        };

        self::onElement($aggregate, $uuid, $duplicateFunction);

        return $aggregate;
    }

    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return PageDuplicateElementCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public function createEvent(CommandInterface $command): EventInterface
    {
        return new PageDuplicateElementEvent($command);
    }

    /**
     * {@inheritdoc}
     *
     * @var Page $aggregate
     */
    public function validateCommand(CommandInterface $command, AggregateInterface $aggregate): bool
    {
        $payload = $command->getPayload();
        // The uuid to duplicate.
        $uuid = $payload['uuid'];
        $element = self::getElement($aggregate, $uuid);

        if (!isset($uuid)) {
            $this->messageBus->dispatch(new Message(
                'No uuid to duplicate is set',
                CODE_BAD_REQUEST,
                $command->getUuid(),
                $command->getAggregateUuid()
            ));

            return false;
        } elseif (!$element) {
            $this->messageBus->dispatch(new Message(
                'Element with this uuid was not found'.$uuid,
                CODE_CONFLICT,
                $command->getUuid(),
                $command->getAggregateUuid()
            ));

            return false;
        } else {
            return true;
        }
    }
}
