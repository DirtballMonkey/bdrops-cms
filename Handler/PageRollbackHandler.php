<?php

declare(strict_types=1);

namespace Bdrops\CMS\Handler;

use Bdrops\CMS\Command\PageRollbackCommand;
use Bdrops\CMS\Event\PageRollbackEvent;
use Bdrops\CMS\Model\Page;
use Bdrops\CQRS\Interfaces\AggregateInterface;
use Bdrops\CQRS\Interfaces\CommandInterface;
use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\HandlerInterface;
use Bdrops\CQRS\Message\Message;

final class PageRollbackHandler extends PageBaseHandler implements HandlerInterface
{
    /**
     * {@inheritdoc}
     *
     * @var Page $aggregate
     */
    public function execute(CommandInterface $command, AggregateInterface $aggregate): AggregateInterface
    {
        $payload = $command->getPayload();

        $previousVersion = $payload['previousVersion'];

        if ($this->aggregateFactory) {
            // Build original aggregate and use its state as a starting point.
            /** @var Page $previousAggregate */
            $previousAggregate = $this->aggregateFactory->build($aggregate->getUuid(), Page::class, intval($previousVersion));

            // Override aggregate meta info.
            $previousAggregate->setVersion($aggregate->getVersion());
            $previousAggregate->setStreamVersion($aggregate->getStreamVersion());
            $previousAggregate->setSnapshotVersion($aggregate->getSnapshotVersion());
            $previousAggregate->setModified(new \DateTimeImmutable());
            $previousAggregate->setHistory($aggregate->getHistory());

            $aggregate = $previousAggregate;
        }

        return $aggregate;
    }

    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return PageRollbackCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public function createEvent(CommandInterface $command): EventInterface
    {
        return new PageRollbackEvent($command);
    }

    /**
     * {@inheritdoc}
     */
    public function validateCommand(CommandInterface $command, AggregateInterface $aggregate): bool
    {
        $payload = $command->getPayload();

        if (
            0 !== $aggregate->getVersion() &&
            isset($payload['previousVersion']) &&
            !empty($payload['previousVersion'])
        ) {
            return true;
        } else {
            $this->messageBus->dispatch(new Message(
                'You must provide a previous version',
                CODE_BAD_REQUEST,
                $command->getUuid(),
                $command->getAggregateUuid()
            ));

            return false;
        }
    }
}
