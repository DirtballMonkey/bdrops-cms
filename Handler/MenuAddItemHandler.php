<?php

declare(strict_types=1);

namespace Bdrops\CMS\Handler;

use Bdrops\CMS\Command\MenuAddItemCommand;
use Bdrops\CMS\Event\MenuAddItemEvent;
use Bdrops\CMS\Model\Menu;
use Bdrops\CQRS\Interfaces\AggregateInterface;
use Bdrops\CQRS\Interfaces\CommandInterface;
use Bdrops\CQRS\Interfaces\EventInterface;
use Bdrops\CQRS\Interfaces\HandlerInterface;
use Bdrops\CQRS\Message\Message;

final class MenuAddItemHandler extends MenuBaseHandler implements HandlerInterface
{
    /**
     * {@inheritdoc}
     *
     * @var Menu $aggregate
     */
    public function execute(CommandInterface $command, AggregateInterface $aggregate): AggregateInterface
    {
        $payload = $command->getPayload();
        $itemName = $payload['itemName'];
        $data = $payload['data'];

        // Build item data.
        $newItem = [
            'uuid' => $command->getUuid(),
            'itemName' => $itemName,
            'data' => $data,
            'enabled' => true,
        ];

        // Add to items.
        $parentUuid = isset($payload['parent']) ? $payload['parent'] : null;

        if ($parentUuid && is_string($parentUuid)) {
            // A function that add the new item to the target parent.
            $addItemFunction = function (&$item, &$collection) use ($newItem) {
                if (!isset($item['items'])) {
                    $item['items'] = [];
                }
                $item['items'][] = $newItem;
            };
            self::onItem($aggregate, $parentUuid, $addItemFunction);
        } else {
            // Add to menu root.
            $aggregate->items[] = $newItem;
        }

        return $aggregate;
    }

    /**
     * {@inheritdoc}
     */
    public static function getCommandClass(): string
    {
        return MenuAddItemCommand::class;
    }

    /**
     * {@inheritdoc}
     */
    public function createEvent(CommandInterface $command): EventInterface
    {
        return new MenuAddItemEvent($command);
    }

    /**
     * {@inheritdoc}
     */
    public function validateCommand(CommandInterface $command, AggregateInterface $aggregate): bool
    {
        $payload = $command->getPayload();

        if (!isset($payload['itemName'])) {
            $this->messageBus->dispatch(new Message(
                'No item type set',
                CODE_BAD_REQUEST,
                $command->getUuid(),
                $command->getAggregateUuid()
            ));

            return false;
        } elseif (!isset($payload['data'])) {
            $this->messageBus->dispatch(new Message(
                'No data set',
                CODE_BAD_REQUEST,
                $command->getUuid(),
                $command->getAggregateUuid()
            ));

            return false;
        } else {
            return true;
        }
    }
}
