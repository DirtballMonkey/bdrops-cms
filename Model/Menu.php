<?php

declare(strict_types=1);

namespace Bdrops\CMS\Model;

use Bdrops\CQRS\Model\Aggregate;

final class Menu extends Aggregate
{
    /** @var string */
    public $name;

    /** @var array */
    public $items;
}
